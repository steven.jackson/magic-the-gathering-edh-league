package com.example.z3r0f.edhleague.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.z3r0f.edhleague.R;

/**
 * Created by z3r0f on 9/7/2019.
 */

public class UserActivity extends AppCompatActivity implements View.OnClickListener {

    Button buttonPlayGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        initViews();
        initListeners();
    }

    private void initViews(){
        buttonPlayGame = findViewById(R.id.buttonPlayGame);
    }

    private void initListeners(){
        buttonPlayGame.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonPlayGame:
                Intent intentPlayGame = new Intent(this, LifeTrackerActivity.class);
                startActivity(intentPlayGame);
                break;
        }
    }
}
