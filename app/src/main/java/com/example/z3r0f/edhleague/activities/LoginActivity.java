package com.example.z3r0f.edhleague.activities;

import android.content.Intent;
import android.renderscript.ScriptGroup;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.z3r0f.edhleague.R;
import com.example.z3r0f.edhleague.helper.InputValidation;
import com.example.z3r0f.edhleague.sql.DatabaseHelper;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private final AppCompatActivity activity = LoginActivity.this;

    private EditText editTextUsername;
    private EditText editTextPassword;

    private Button buttonLogin;
    private TextView textViewCreateAccount;

    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();
        initListeners();
        initObjects();
    }

    private void initViews(){
        editTextUsername = (EditText) findViewById(R.id.editTextUsername);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        textViewCreateAccount = (TextView) findViewById(R.id.textViewCreateAccount);
    }

    private void initListeners(){
        buttonLogin.setOnClickListener(this);
        textViewCreateAccount.setOnClickListener(this);
    }

    private void initObjects(){
        databaseHelper = new DatabaseHelper(activity);
        inputValidation = new InputValidation(activity);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonLogin:
                verifyUser();
                break;
            case R.id.textViewCreateAccount:
                Intent intentRegister = new Intent(this, RegisterActivity.class);
                startActivity(intentRegister);
                break;
        }
    }

    private void verifyUser(){
        if(!inputValidation.isEditTextFilled(editTextUsername)){
            Toast.makeText(activity, R.string.username_required, Toast.LENGTH_SHORT).show();
            return;
        }
        if(!inputValidation.isEditTextFilled(editTextPassword)){
            Toast.makeText(activity, R.string.password_required, Toast.LENGTH_SHORT).show();
            return;
        }

        if(databaseHelper.checkUser(editTextUsername.getText().toString().trim()
                , null
                , editTextPassword.getText().toString().trim())){
            Intent accountIntent = new Intent(activity, UserActivity.class);
            emptyEditText();
            startActivity(accountIntent);
        }
        else{
            Toast.makeText(activity, R.string.invalid_username_password, Toast.LENGTH_SHORT).show();
        }
    }

    private void emptyEditText(){
        editTextUsername.setText(null);
        editTextPassword.setText(null);
    }
}
