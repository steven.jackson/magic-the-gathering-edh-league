package com.example.z3r0f.edhleague.helper;

import android.content.Context;
import android.util.Patterns;
import android.widget.EditText;

/**
 * Created by z3r0f on 9/7/2019.
 */

public class InputValidation {

    private Context context;

    public InputValidation(Context context){
        this.context = context;
    }

    public boolean isEditTextFilled(EditText editText){
        String editTextField = editText.getText().toString().trim();
        if(editTextField.isEmpty()){
            return false;
        }
        return true;
    }

    public boolean isEditTextEmail(EditText editText){
        String editTextField = editText.getText().toString().trim();
        if(editTextField.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(editTextField).matches()){
            return false;
        }
        return true;
    }

    public boolean isEditTextMatches(EditText editText1, EditText editText2){
        String editTextField1 = editText1.getText().toString().trim();
        String editTextField2 = editText2.getText().toString().trim();
        if(!editTextField1.contentEquals(editTextField2)){
            return false;
        }
        return true;
    }
}
