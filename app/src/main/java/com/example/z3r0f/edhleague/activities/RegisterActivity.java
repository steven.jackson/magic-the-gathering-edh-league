package com.example.z3r0f.edhleague.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.z3r0f.edhleague.R;
import com.example.z3r0f.edhleague.helper.InputValidation;
import com.example.z3r0f.edhleague.model.User;
import com.example.z3r0f.edhleague.sql.DatabaseHelper;

/**
 * Created by z3r0f on 9/6/2019.
 */

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private final AppCompatActivity activity = RegisterActivity.this;

    private EditText editTextUsername;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextConfirmPassword;

    private Button buttonRegister;
    private TextView textViewLogin;

    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initViews();
        initListeners();
        initObjects();
    }

    private void initViews(){
        editTextUsername = (EditText) findViewById(R.id.editTextUsername);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextConfirmPassword = (EditText) findViewById(R.id.editTextConfirmPassword);
        buttonRegister = (Button) findViewById(R.id.buttonLogin);
        textViewLogin = (TextView) findViewById(R.id.textViewCreateAccount);
    }

    private void initListeners(){
        buttonRegister.setOnClickListener(this);
        textViewLogin.setOnClickListener(this);
    }

    private void initObjects(){
        inputValidation = new InputValidation(activity);
        databaseHelper = new DatabaseHelper(activity);
        user = new User();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonLogin:
                createUser();
                break;
            case R.id.textViewCreateAccount:
                finish();
                break;
        }
    }

    private void createUser(){
        if(!inputValidation.isEditTextFilled(editTextUsername)){
            Toast.makeText(activity, R.string.username_required, Toast.LENGTH_SHORT).show();
            return;
        }
        if(!inputValidation.isEditTextFilled(editTextEmail)){
            Toast.makeText(activity, R.string.email_required, Toast.LENGTH_SHORT).show();
            return;
        }
        if(!inputValidation.isEditTextEmail(editTextEmail)){
            Toast.makeText(activity, R.string.email_not_valid, Toast.LENGTH_SHORT).show();
            return;
        }
        if(!inputValidation.isEditTextFilled(editTextPassword)){
            Toast.makeText(activity, R.string.password_required, Toast.LENGTH_SHORT).show();
            return;
        }
        if(!inputValidation.isEditTextMatches(editTextPassword, editTextConfirmPassword)){
            Toast.makeText(activity, R.string.passwords_not_matching, Toast.LENGTH_SHORT).show();
            return;
        }

        if(!databaseHelper.checkUser(editTextUsername.getText().toString().trim()
                , editTextEmail.getText().toString().trim()
                , null)){
            user.setUsername(editTextUsername.getText().toString().trim());
            user.setEmail(editTextEmail.getText().toString().trim());
            user.setPassword(editTextPassword.getText().toString().trim());

            databaseHelper.addUser(user);

            Toast.makeText(activity, R.string.account_created, Toast.LENGTH_SHORT).show();
            emptyEditText();
        }
        else{
            Toast.makeText(activity, R.string.username_or_email_already_in_use, Toast.LENGTH_SHORT).show();
        }
    }

    private void emptyEditText(){
        editTextUsername.setText(null);
        editTextEmail.setText(null);
        editTextPassword.setText(null);
        editTextConfirmPassword.setText(null);
    }
}
