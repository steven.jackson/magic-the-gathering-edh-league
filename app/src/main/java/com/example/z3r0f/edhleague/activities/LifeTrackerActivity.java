package com.example.z3r0f.edhleague.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.z3r0f.edhleague.R;

/**
 * Created by z3r0f on 9/9/2019.
 */

public class LifeTrackerActivity extends AppCompatActivity implements View.OnClickListener {

    Button decP1, incP1, decP2, incP2;
    TextView lifeP1, lifeP2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        requestWindowFeature( Window.FEATURE_NO_TITLE );
//        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN );
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_tracker);

        initViews();
        initListeners();
        lifeP1.setText("40");
        lifeP2.setText("40");
    }

    private void initViews(){
        decP1 = findViewById(R.id.buttonDecP1);
        incP1 = findViewById(R.id.buttonIncP1);
        decP2 = findViewById(R.id.buttonDecP2);
        incP2 = findViewById(R.id.buttonIncP2);

        lifeP1 = findViewById(R.id.editTextLifeP1);
        lifeP2 = findViewById(R.id.editTextLifeP2);
    }

    private void initListeners(){
        decP1.setOnClickListener(this);
        incP1.setOnClickListener(this);
        decP2.setOnClickListener(this);
        incP2.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonDecP1:
                lifeP1.setText(String.valueOf(Integer.valueOf(lifeP1.getText().toString()) - 1));
                break;
            case R.id.buttonIncP1:
                lifeP1.setText(String.valueOf(Integer.valueOf(lifeP1.getText().toString()) + 1));
                break;
            case R.id.buttonDecP2:
                lifeP2.setText(String.valueOf(Integer.valueOf(lifeP2.getText().toString()) - 1));
                break;
            case R.id.buttonIncP2:
                lifeP2.setText(String.valueOf(Integer.valueOf(lifeP2.getText().toString()) + 1));
                break;
        }
    }
}
